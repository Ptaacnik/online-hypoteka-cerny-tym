import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',  
  styleUrls: ['./slider.component.scss']
})

export class SliderComponent implements OnInit {
  @Input()
  value:number;
  @Input()
  max:number;
  @Input()
  min:number;
  @Output()
  onChange: EventEmitter<number> = new EventEmitter();

  ngOnInit() {
  }

  change(event){
    this.onChange.emit(event.value);
  }
}
