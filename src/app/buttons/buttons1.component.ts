import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Component({
    selector: 'app-buttons1',
    templateUrl: './buttons1.component.html',  
    styleUrls: ['./buttons.component.scss']
})

export class Buttons1Component implements OnInit{
  @Input()
  value: boolean = false;
  @Output()
  onChange: EventEmitter<number> = new EventEmitter();

  ngOnInit() {
  }

  types: SelectItem[];
  constructor() { 
      this.types = [
      {label: "Ano", value: true},
      {label: "Ne", value: false}
      ]   
  }
  handleChange(event){
    this.onChange.emit(event.value);
  }
}