import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Component({
    selector: 'app-buttons',
    templateUrl: './buttons.component.html',  
    styleUrls: ['./buttons.component.scss']
})

export class ButtonsComponent implements OnInit{
  @Input()
  value: number = 3;
  @Output()
  onChange: EventEmitter<number> = new EventEmitter();

  ngOnInit() {
  }

  types: SelectItem[];
  constructor() { 
      this.types = [
      {label: "1", value: 1},
      {label: "3", value: 3},
      {label: "5", value: 5},
      {label: "7", value: 7}
      ]   
  }
  change(event){
    this.onChange.emit(event.value);
  }
}