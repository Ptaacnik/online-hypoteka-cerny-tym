import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-toggleButton',
    templateUrl: './toggleButton.component.html',  
    styleUrls: ['./toggleButton.component.scss']
})

export class ToggleButtonComponent{
  @Input()
  checked: boolean = false;
  @Output()
  onChange: EventEmitter<number> = new EventEmitter();

  handleChange($event){
    this.onChange.emit($event.checked)
  }
}