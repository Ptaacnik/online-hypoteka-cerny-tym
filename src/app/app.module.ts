import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {SliderComponent} from "./slider/slider.component";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SliderModule} from 'primeng/slider';
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {AccordionModule} from 'primeng/accordion';
import {AccordionComponent} from  "./accordion/accordion.component";
import {SelectButtonModule} from 'primeng/selectbutton';
import {ButtonsComponent} from "./buttons/buttons.component";
import {Buttons1Component} from "./buttons/buttons1.component";
import {ToggleButtonModule} from 'primeng/togglebutton';
import {ToggleButtonComponent} from "./toggleButton/toggleButton.component";
import {DigitOnlyDirective} from "./digit-only.directive";
import {DigitOnlyDirective2} from "./digit-only.directive2";


@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    AccordionComponent,
    ButtonsComponent,
    Buttons1Component,
    ToggleButtonComponent,
    DigitOnlyDirective,
    DigitOnlyDirective2
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    SliderModule,
    ToastModule,
    AccordionModule,
    SelectButtonModule,
    ToggleButtonModule
  ],
  providers: [MessageService],
  exports: [DigitOnlyDirective, DigitOnlyDirective2],
  bootstrap: [AppComponent]
})
export class AppModule { }
