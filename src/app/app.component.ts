import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  values: number[] = [10,1500000];
  mins: number[] = [5,400000];
  maxs: number[] = [30, 5000000];
  checkeds: boolean = false;

  defUroksazba: number = 3;
  defZvyhpojist: number = 0.5;


  uroksazba: number = this.localValue("usazbaLS", this.defUroksazba);
  zvyhpojist: number = this.localValue("zvyhpojistLS", this.defZvyhpojist);
  minlet: number = this.localValue("minletLS", this.mins[0]);
  maxlet: number = this.localValue("maxletLS", this.maxs[0]);
  minpujcka: number = this.localValue("minpujckaLS", this.mins[1]);
  maxpujcka: number = this.localValue("maxpujckaLS", this.maxs[1]);
  vlastniZdroje: number = 10000;
  prijemNaDomacnost: number = 30000;
  
  logins: Map<string, string>;
  username: string;
  psw: string;
  adminCheck = false;
  

  constructor() {  
    this.logins = new Map<string, string>();  
    this.logins.set("tester", "Heslo0001");
  }
  


  pujcka(p1: number, p2: number) {
    return Math.round(Math.min(p1*(100/20),(p2/100)*50*30*12));
  }

  splatka(p1: number, p2: number, p3: number){
   return Math.round((((p3/100)/12)*p2)/(1-Math.pow(1/(1+((p3/100)/12)),p1*12)));
  }

  usazba(p1:number, p2:boolean){
    var x = this.uroksazba;
    if(p1 == 1){
      x += 0.5;
    }
    if(p2){
      x -= this.zvyhpojist;
    }
    return x;
  }

  celkem(p1: number, p2: number, p3: number){
    return Math.round((((p3/100)/12)*p2)/(1-Math.pow(1/(1+((p3/100)/12)),p1*12))*p1*12)
  }

  localValue(p1: string, p2: number){
    if(localStorage.getItem(p1) != null){
      return parseInt(localStorage.getItem(p1));
    } else {
      return p2;
    }
  }

  loginAction(p1, p2){
    if(p1 == null || p2 == null) {
      console.log("nope");
    } else {
      if(this.logins.get(p1) == p2) {
        document.getElementById('login').style.display='none';
        document.getElementById('admin').style.display='block';
        document.getElementById('admin').scrollIntoView();
        this.adminCheck = true;
        return true;
      } else {
        this.psw = null;
      }
    }
  }

  clickAction() {
    if(this.adminCheck) {
      document.getElementById('admin').scrollIntoView();
      return false;
    } else {
      document.getElementById('login').style.display='block';
      return true;
    }
  }

  clickCancel(){
    document.getElementById('login').style.display='none'; 
    this.psw = null;
  }
  
  change(event, index){
    this.values[index] = event;
    if (index == 1){
      document.getElementById("pujckaTT").style.background = "#d1d1d1";
      document.getElementById("pujckaTT").style.color = "#000000";
    }
    if (index == 0){
      document.getElementById("letTT").style.background = "#d1d1d1";
      document.getElementById("letTT").style.color = "#000000";
    }
  }
  handleChange(event){
    this.checkeds = event;
  }
  changeLocalS(p1: string, p2: number) {
    localStorage.setItem(p1, p2.toString());
  }

  // When the user clicks anywhere outside of the modal, close it
  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent){
    var modal = document.getElementById('login');
    if (event.target == modal) {
      modal.style.display = "none";
      this.psw = null;
    }
  }

  tooltips: string[] = [
    "Uveďte, kolik peněz si chcete od nás půjčit.",
    "Uveďte, kolik let chcete hypotéku zplácet.",
    "Období, pro které je úrok nastaven na určitou hodnotu a není možné ho změnit.",
    "Pojištění proti neschopnosti splácet hypotéku.",
    "Částka, kterou budete měsíčně platit.",
    "Úroková sazba je procentuální vyjádření ceny za poskytnutí hypotéky.",
    "Částka, kterou celkově zaplatíte.",
    "Uveďte, kolik peněz máte na nemovitost naspořeno z vlastních zdrojů.",
    "Uveďte, jaký je Váš měsíční příjem pro celou domácnost.",
    "Maximální orientační částka, kterou Vám můžeme půjčit.",

    "Nastavení nové hodnoty úrokové sazby. Hodnota musí být nezáporná, větší než zvýhodnění pojištění, nesmí být větší jak 100% a nesmí obsahovat nečíselné znaky (vyjma jedné desetinné čárky).",
    "Nastavení nové hodnoty zvýhodnění pojištění. Hodnota musí být nezáporná, menší než úroková sazba, nesmí být větší jak 100% a nesmí obsahovat nečíselné znaky (vyjma jedné desetinné čárky).",
    "Nastavení nové minimální hodnoty. Hodnota nesmí být větší než je maximální hodnota, nesmí být záporná a nesmí obsahovat nečíselné znaky.",
    "Nastavení nové maximální hodnoty. Hodnota nesmí být menší než je minimální hodnota, nesmí být záporná a nesmí obsahovat nečíselné znaky.",
    "Nastavení nové minimální hodnoty. Hodnota nesmí být větší než je maximální hodnota, nesmí být záporná a nesmí obsahovat nečíselné znaky.",
    "Nastavení nové maximální hodnoty. Hodnota nesmí být menší než je minimální hodnota, nesmí být záporná a nesmí obsahovat nečíselné znaky."
  ];

  vlastniZdrojeValid() {
    if(this.vlastniZdroje <= 0){
      document.getElementById("vlastniZdrojeTT").style.background = "#ff0000";
      document.getElementById("vlastniZdrojeTT").style.color = "#ffffff";
      this.vlastniZdroje = 1;
    } else {
      document.getElementById("vlastniZdrojeTT").style.background = "#d1d1d1";
      document.getElementById("vlastniZdrojeTT").style.color = "#000000";
    }
  }

  prijemNaDomacnostValid() {
    if(this.prijemNaDomacnost <= 0){
      document.getElementById("prijemNaDomacnostTT").style.background = "#ff0000";
      document.getElementById("prijemNaDomacnostTT").style.color = "#ffffff";
      this.prijemNaDomacnost = 1;
    } else {
      document.getElementById("prijemNaDomacnostTT").style.background = "#d1d1d1";
      document.getElementById("prijemNaDomacnostTT").style.color = "#000000";
    }
  }

  pujckaValid() {
    if(this.values[1] >= this.maxpujcka){
      document.getElementById("pujckaTT").style.background = "#ff0000";
      document.getElementById("pujckaTT").style.color = "#ffffff";
      this.values[1] = this.maxpujcka;
    } else if(this.values[1] <= this.minpujcka){
      document.getElementById("pujckaTT").style.background = "#ff0000";
      document.getElementById("pujckaTT").style.color = "#ffffff";
      this.values[1] = this.minpujcka;
    } else {
      document.getElementById("pujckaTT").style.background = "#d1d1d1";
      document.getElementById("pujckaTT").style.color = "#000000";
    }
  }

  letValid() {
    if(this.values[0] >= this.maxlet){
      document.getElementById("letTT").style.background = "#ff0000";
      document.getElementById("letTT").style.color = "#ffffff";
      this.values[0] = this.maxlet;
    } else if(this.values[0] <= this.minlet){
      document.getElementById("letTT").style.background = "#ff0000";
      document.getElementById("letTT").style.color = "#ffffff";
      this.values[0] = this.minlet;
    } else {
      document.getElementById("letTT").style.background = "#d1d1d1";
      document.getElementById("letTT").style.color = "#000000";
    }
  }

  uroksazbaValid() {
    if (this.uroksazba <= this.zvyhpojist){
      document.getElementById("uroksazbaTT").style.background = "#ff0000";
      document.getElementById("uroksazbaTT").style.color = "#ffffff";
      this.uroksazba = this.zvyhpojist + 0.5;
    } else if (this.uroksazba <= 0){
      document.getElementById("uroksazbaTT").style.background = "#ff0000";
      document.getElementById("uroksazbaTT").style.color = "#ffffff";
      this.uroksazba = 1;
    } else {
      document.getElementById("uroksazbaTT").style.background = "#d1d1d1";
      document.getElementById("uroksazbaTT").style.color = "#000000";
    }
  }

  zvyhpojistValid() {
    if (this.zvyhpojist >= this.uroksazba){
      document.getElementById("zvyhpojistTT").style.background = "#ff0000";
      document.getElementById("zvyhpojistTT").style.color = "#ffffff";
      this.zvyhpojist = this.uroksazba - 0.5;
    } else if (this.zvyhpojist <= 0){
      document.getElementById("zvyhpojistTT").style.background = "#ff0000";
      document.getElementById("zvyhpojistTT").style.color = "#ffffff";
      this.zvyhpojist = 0.5;
    } else {
      document.getElementById("zvyhpojistTT").style.background = "#d1d1d1";
      document.getElementById("zvyhpojistTT").style.color = "#000000";
    }
  }

  minpujckaValid() {
    if (this.minpujcka >= this.maxpujcka){
      document.getElementById("minpujckaTT").style.background = "#ff0000";
      document.getElementById("minpujckaTT").style.color = "#ffffff";
      this.minpujcka = this.maxpujcka - 1;
    } else if (this.minpujcka <= 0){
      document.getElementById("minpujckaTT").style.background = "#ff0000";
      document.getElementById("minpujckaTT").style.color = "#ffffff";
      this.minpujcka = 1;
    } else {
      document.getElementById("minpujckaTT").style.background = "#d1d1d1";
      document.getElementById("minpujckaTT").style.color = "#000000";
    }
  }

  maxpujckaValid() {
    if (this.maxpujcka <= this.minpujcka){
      document.getElementById("maxpujckaTT").style.background = "#ff0000";
      document.getElementById("maxpujckaTT").style.color = "#ffffff";
      this.maxpujcka = this.minpujcka + 1;
    } else if (this.maxpujcka <= 0){
      document.getElementById("maxpujckaTT").style.background = "#ff0000";
      document.getElementById("maxpujckaTT").style.color = "#ffffff";
      this.maxpujcka = 1;
    } else {
      document.getElementById("maxpujckaTT").style.background = "#d1d1d1";
      document.getElementById("maxpujckaTT").style.color = "#000000";
    }
  }

  minletValid() {
    if (this.minlet >= this.maxlet){
      document.getElementById("minletTT").style.background = "#0000ff";
      document.getElementById("minletTT").style.color = "#ffffff";
      this.minlet = this.maxlet - 1;
    } else if (this.minlet <= 0){
      document.getElementById("minletTT").style.background = "#0000ff";
      document.getElementById("minletTT").style.color = "#ffffff";
      this.minlet = 1;
    } else {
      document.getElementById("minletTT").style.background = "#d1d1d1";
      document.getElementById("minletTT").style.color = "#000000";
    }
  }

  maxletValid() {
    if (this.maxlet <= this.minlet){
      document.getElementById("maxletTT").style.background = "#ff0000";
      document.getElementById("maxletTT").style.color = "#ffffff";
      this.maxlet = this.minlet + 1;
    } else if (this.maxlet <= 0){
      document.getElementById("maxletTT").style.background = "#ff0000";
      document.getElementById("maxletTT").style.color = "#ffffff";
      this.maxlet = 1;
    } else {
      document.getElementById("maxletTT").style.background = "#d1d1d1";
      document.getElementById("maxletTT").style.color = "#000000";
    }
  }

}
